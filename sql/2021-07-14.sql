ALTER TABLE spider_config ADD COLUMN save_db SMALLINT(6) DEFAULT 0 COMMENT '保存到数据库';
ALTER TABLE spider_config ADD COLUMN spider_high_setting VARCHAR(100) DEFAULT NULL COMMENT '爬虫高级设置';


INSERT INTO `sys_dict_type` VALUES ('148', '爬虫高级设置', 'spider_high_setting', '0', 'admin', '2021-07-14 13:30:36', '', null, null);
INSERT INTO `sys_dict_data` VALUES ('305', '4', '替换掉html标签', 'replace_html', 'field_value_process_type', null, null, 'N', '0', 'admin', '2021-07-14 10:46:33', '', null, null);
INSERT INTO `sys_dict_data` VALUES ('306', '5', '替换掉a标签', 'replace_a', 'field_value_process_type', '', '', 'N', '0', 'admin', '2021-07-14 11:25:47', 'admin', '2021-07-14 13:20:01', '');
INSERT INTO `sys_dict_data` VALUES ('307', '1', '下载单张图片', '1', 'spider_high_setting', null, null, 'N', '0', 'admin', '2021-07-14 13:32:51', '', null, null);
INSERT INTO `sys_dict_data` VALUES ('308', '2', '下载内容详情图片', '2', 'spider_high_setting', null, null, 'N', '0', 'admin', '2021-07-14 13:33:19', '', null, null);
INSERT INTO `sys_dict_data` VALUES ('309', '3', '自动选取封面图片', '3', 'spider_high_setting', null, null, 'N', '0', 'admin', '2021-07-14 13:33:45', '', null, null);